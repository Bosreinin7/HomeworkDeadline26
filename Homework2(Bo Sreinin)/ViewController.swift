//
//  ViewController.swift
//  Homework2(Bo Sreinin)
//
//  Created by SreiNin Bo on 11/25/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class ViewController: UIViewController , UITextFieldDelegate{

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var passWordTextField: UITextField!
    @IBOutlet weak var emailTexField: UITextField!
    @IBOutlet weak var phoneNumberTextField: UITextField!
    
   
    @IBOutlet var signUpButton: UIButton!
    var label:UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        userNameTextField.delegate = self
        passWordTextField.delegate = self
        phoneNumberTextField.delegate = self
        emailTexField.delegate = self
        
        let headLabel = UILabel(frame: CGRect(x: 0, y: 0, width: 50, height: 20))
        headLabel.text = "(+855)"
        // Style for all TextFields
        func textFieldStyle(textField:UITextField){
            textField.borderStyle = .roundedRect
            textField.layer.borderColor = UIColor.white.cgColor
            
        }
        
        textFieldStyle(textField: userNameTextField)
        textFieldStyle(textField: passWordTextField)
        textFieldStyle(textField: phoneNumberTextField)
        textFieldStyle(textField: emailTexField)
        phoneNumberTextField.addTarget(self, action: #selector(textFieldDidEndEditing(_:)), for: .editingChanged)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showData"{
            let dataShow:SecondViewController = segue.destination as! SecondViewController
            let data = userNameTextField.text!
            print("data is\(data)")
            dataShow.dataGet = "Welcome to \(data.uppercased()) , Your account has been created"
        }
    }
    
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField.tag == 3  {
            label = UILabel(frame:CGRect(x: 0, y: 0, width:46, height: 30))
            label.text = "(+855)"
            label.font = UIFont.systemFont(ofSize: 14.0)
            phoneNumberTextField.placeholder = "096-929-9959"
            phoneNumberTextField.leftViewMode = UITextFieldViewMode.always
            phoneNumberTextField.leftView = label
        }
        
        
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        // to hide (+855)
        if textField.tag == 3 && textField.text == "" {
            label.isHidden = true
            label.frame = CGRect.zero
            phoneNumberTextField.placeholder = "(+855) 096-929-9959"
        }
        // Formatting phone number
        if textField.tag == 3 && textField.text?.count == 3 || textField.text?.count == 7  {
            textField.text! += "-"
        }
        
        if textField.tag == 3 && textField.text?.count == 12 {
            textField.endEditing(false)
            textField.resignFirstResponder()
            emailTexField.becomeFirstResponder()
        }
        
        
    }
    
    // Customize key(Next,Done)
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField.tag < passWordTextField.tag {
            userNameTextField.resignFirstResponder()
            passWordTextField.becomeFirstResponder()
        }
        else if textField.tag < phoneNumberTextField.tag{
            passWordTextField.resignFirstResponder()
            phoneNumberTextField.becomeFirstResponder()
        }
        else {
            emailTexField.resignFirstResponder()
            
        }
        
        return true
    }
    // Go back screen
    @IBAction func backScreen(_ sender:UIStoryboardSegue){
        userNameTextField.text = ""
        passWordTextField.text = ""
        phoneNumberTextField.text = ""
        emailTexField.text = ""
        label.isHidden = true
        label.frame = CGRect.zero
         phoneNumberTextField.placeholder = "(+855) 096-929-9959"
        
        
    }



}

