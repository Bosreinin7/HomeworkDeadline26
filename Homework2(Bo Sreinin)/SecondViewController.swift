//
//  SecondViewController.swift
//  Homework2(Bo Sreinin)
//
//  Created by SreiNin Bo on 11/25/17.
//  Copyright © 2017 SreiNin Bo. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    
    @IBOutlet var greetingLabel: UILabel!
    @IBOutlet var imageView: UIImageView!
    var dataGet:String?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageView?.image =  UIImage(named: "greeting.png")
        greetingLabel.text = dataGet
    }

}
